//
//  CatsRule.swift
//  CatsRule
//
//  Created by Ben Thomas on 11/3/18.
//

import XCTest
@testable import CatsRule

class RootViewModelTests: XCTestCase {
    func testViewModelInitialState() {
        let rootViewModel = RootViewModel()

        XCTAssert(rootViewModel.numberOfSections == 0)
        XCTAssertNil(rootViewModel.numberOfRows(in: 0))
        XCTAssertNil(rootViewModel.title(for: 0))
        XCTAssertNil(rootViewModel.cellViewModelForRow(at: IndexPath(row: 0, section: 0)))
    }

    func testViewModelAfterNetworkSuccess() {
        let rootViewModel = RootViewModel()

        let successExpectation = expectation(description: "Correct root view model returned from view model when network response is success")

        // this test will be flaky because the data is stored in a dictionary, where order is not guaranteed to be consistent
        // and we're not mocking the response so the data could change if the API changes

        let success = {
            XCTAssertEqual(rootViewModel.numberOfSections, 2)
            XCTAssertEqual(rootViewModel.numberOfRows(in: 0), 4)
            XCTAssertEqual(rootViewModel.title(for: 0), "Male")
            XCTAssertEqual(rootViewModel.title(for: 1), "Female")

            let rootCellViewModel1 = rootViewModel.cellViewModelForRow(at: IndexPath(row: 0, section: 1))
            let rootCellViewModel2 = rootViewModel.cellViewModelForRow(at: IndexPath(row: 2, section: 0))

            XCTAssertEqual(rootCellViewModel1?.catName, "Garfield")
            XCTAssertEqual(rootCellViewModel2?.catName, "Max")

            XCTAssertNil(rootViewModel.title(for: 2))

            successExpectation.fulfill()
        }

        let failure: (String) -> () = { errorDescription in
            XCTFail()
        }

        rootViewModel.getCats(success: success, failure: failure)

        waitForExpectations(timeout: 10) { error in
            if let error = error {
                XCTFail("waitForExpectationsWithTimeout errored: \(error)")
            }
        }
    }
}
