//
//  PetMapperTests.swift
//  UnitTests
//
//  Created by Ben Thomas on 12/3/18.
//

import XCTest
@testable import CatsRule

class PetModelMapperTests: XCTestCase {
    var responseModel: [PersonModel]!
    
    let seansPets = [PetModel(name: "Spot", type: "Dog"), PetModel(name: "Dory", type: "Fish"), PetModel(name: "Rex", type: "Dinosaur"), PetModel(name: "Mr Scratches", type: "Cat")]
    let mandiesPets = [PetModel(name: "Fido", type: "Dog"), PetModel(name: "Nemo", type: "Fish"), PetModel(name: "Sylvester", type: "Cat")]
    let oliversPets: [PetModel]? = nil
    let petesPets = [PetModel(name: "Darth", type: "Cat"), PetModel(name: "Fluffy", type: "Cat"), PetModel(name: "Boofy", type: "Cat")]
    let petsOfVlas = [PetModel(name: "Ginger", type: "Giraffe")]
    let kathiesPets = [PetModel(name: "Flash", type: "Cat"), PetModel(name: "Snowy", type: "Cat")]
    let kimsPets = [PetModel(name: "Tiger", type: "Cat"), PetModel(name: "Fido", type: "Dog")]


    override func setUp() {
        let sean = PersonModel(gender: "Male", pets: seansPets)
        let mandy = PersonModel(gender: "Female", pets: mandiesPets)
        let oliver = PersonModel(gender: "Male", pets: oliversPets)
        let pete = PersonModel(gender: "Male", pets: petesPets)
        let vlas = PersonModel(gender: "Male", pets: petsOfVlas)
        let kathy = PersonModel(gender: "Female", pets: kathiesPets)
        let kim = PersonModel(gender: nil, pets: kimsPets)
        responseModel = [sean, mandy, oliver, pete, vlas, kathy, kim]
    }

    func testPetModelMapperShouldOnlyIncludeCats() {
        let petModelMapper = PetModelMapper()
        let cats = petModelMapper.getCatsOnly(from: seansPets)

        XCTAssertEqual(cats!, ["Mr Scratches"])
    }
    
    func testResponseModelShouldMapToGenderModel() {
        let petModelMapper = PetModelMapper()

        let genderModel = petModelMapper.map(responseModel: responseModel)
        XCTAssertEqual(genderModel.count, 3)
        XCTAssertEqual(genderModel.safelyGetValue(at: 0)!.value.count, 1)
        XCTAssertEqual(genderModel.safelyGetValue(at: 1)!.value.count, 4)
        XCTAssertEqual(genderModel.safelyGetValue(at: 2)!.value.count, 3)

        let expectedCatsOfFemales = ["Sylvester", "Flash", "Snowy"]
        XCTAssertEqual(genderModel.safelyGetValue(at: 2)!.value, expectedCatsOfFemales)

        let expectedCatsOfMales = ["Mr Scratches", "Darth", "Fluffy", "Boofy"]
        XCTAssertEqual(genderModel.safelyGetValue(at: 1)!.value, expectedCatsOfMales)

        let expectedCatsOfPeopleOfUnknownGender = ["Tiger"]
        XCTAssertEqual(genderModel.safelyGetValue(at: 0)!.value, expectedCatsOfPeopleOfUnknownGender)

    }

    func testGenderModelShouldContainCatNamesSortedAlphabetically() {
        let petModelMapper = PetModelMapper()

        let genderModel = petModelMapper.map(responseModel: responseModel)
        let genderModelWithSortedCatNames = petModelMapper.getDataModelWithSortedCatNames(in: genderModel)

        XCTAssertEqual(genderModelWithSortedCatNames.count, 3)
        XCTAssertEqual(genderModelWithSortedCatNames.safelyGetValue(at: 0)!.value.count, 1)
        XCTAssertEqual(genderModelWithSortedCatNames.safelyGetValue(at: 1)!.value.count, 4)
        XCTAssertEqual(genderModelWithSortedCatNames.safelyGetValue(at: 2)!.value.count, 3)

        let expectedSortedCatsOfFemales = ["Flash", "Snowy", "Sylvester"]
        XCTAssertEqual(genderModelWithSortedCatNames.safelyGetValue(at: 2)!.value, expectedSortedCatsOfFemales)

        let expectedSortedCatsOfMales = ["Boofy", "Darth", "Fluffy", "Mr Scratches"]
        XCTAssertEqual(genderModelWithSortedCatNames.safelyGetValue(at: 1)!.value, expectedSortedCatsOfMales)

        let expectedCatsOfPeopleOfUnknownGender = ["Tiger"]
        XCTAssertEqual(genderModelWithSortedCatNames.safelyGetValue(at: 0)!.value, expectedCatsOfPeopleOfUnknownGender)
    }
}
