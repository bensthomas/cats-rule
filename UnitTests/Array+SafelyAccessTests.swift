//
//  Array+SafelyAccessTests.swift
//  UnitTests
//
//  Created by Ben Thomas on 12/3/18.
//  Copyright © 2018 Ben Thomas. All rights reserved.
//

import XCTest
@testable import CatsRule

class Array_SafelyAccessTests: XCTestCase {
    
    let array = ["a", "b", "c"]

    func testShouldRetrieveValueAtValidIndex() {
        XCTAssertEqual(array.safelyGetValue(at: 0), "a")
        XCTAssertEqual(array.safelyGetValue(at: 1), "b")
        XCTAssertEqual(array.safelyGetValue(at: 2), "c")
    }

    func testShouldNotCrashWhenAttemptingToRetrieveFromInvalidIndex() {
        XCTAssertNil(array.safelyGetValue(at: 3))
    }
}
