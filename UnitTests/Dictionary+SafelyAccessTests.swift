//
//  Dictionary+SafelyAccessTests.swift
//  UnitTests
//
//  Created by Ben Thomas on 12/3/18.
//  Copyright © 2018 Ben Thomas. All rights reserved.
//

import XCTest
@testable import CatsRule

class Dictionary_SafelyAccessTests: XCTestCase {
    let dictionary = ["a": "aString", "b": "anotherString"]

    func testShouldRetrieveKeyAtIndex() {
        // bogus, because order of dictionary is not guaranteed
        XCTAssertEqual(dictionary.safelyGetKey(at: 1), "a")
    }

    func testShouldNotCrashWhenAttemptingToRetrieveKeyFromInvalidIndex() {
        XCTAssertNil(dictionary.safelyGetKey(at: 2))
    }

    func testShouldRetrieveValueAtIndex() {
        // bogus, because order of dictionary is not guaranteed
        XCTAssertEqual(dictionary.safelyGetValue(at: 1), "aString")
    }

    func testShouldNotCrashWhenAttemptingToRetrieveValueFromInvalidIndex() {
        XCTAssertNil(dictionary.safelyGetKey(at: 2))
    }
}
