//
//  APIClient.swift
//  CatsRule
//
//  Created by Ben Thomas on 11/3/18.
//

import Foundation
import Alamofire
import AlamofireNetworkActivityIndicator

class APIClient {
    public static let shared = APIClient()

    private init() {
        NetworkActivityIndicatorManager.shared.isEnabled = true
    }

    func get(from endPoint: Endpoint, success: @escaping (Data) -> (), failure: @escaping (Error) -> ()) {
        guard let url = endPoint.url else {
            assertionFailure()
            return
        }

        Alamofire.request(url).responseData { response in
            switch response.result {
            case .success(let value):
                success(value)
            case .failure(let error):
                failure(error)
            }
        }
    }
}
