//
//  PetModel.swift
//  CatsRule
//
//  Created by Ben Thomas on 12/3/18.
//

import Foundation

struct PetModel: Decodable {
    let name: String?
    let type: String?
}
