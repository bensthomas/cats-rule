//
//  PersonModel.swift
//  CatsRule
//
//  Created by Ben Thomas on 11/3/18.
//

import Foundation

struct PersonModel: Decodable {
    let gender: String?
    let pets: [PetModel]?
}
