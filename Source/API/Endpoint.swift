//
//  Endpoint.swift
//  CatsRule
//
//  Created by Ben Thomas on 12/3/18.
//

import Foundation

enum Endpoint {
    case petOwners

    var host: String {
        switch self {
        case .petOwners:
            return "http://agl-developer-test.azurewebsites.net"
        }
    }

    var path: String {
        switch self {
        case .petOwners:
            return "/people.json"
        }
    }

    var url: URL? {
        return URL(string: path, relativeTo: URL(string: host))
    }
}
