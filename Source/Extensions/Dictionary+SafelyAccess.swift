//
//  Dictionary+SafelyAccess.swift
//  CatsRule
//
//  Created by Ben Thomas on 12/3/18.
//

import Foundation

extension Dictionary {
    func safelyGetKey<Key>(at index: Int) -> Key? {
        let keys = Array(self.keys)
        guard keys.indices.contains(index) else { return nil }
        return keys[index] as? Key
    }

    func safelyGetValue(at index: Int) -> Value? {
        let values = Array(self.values)
        guard values.indices.contains(index) else { return nil }
        return values[index]
    }
}
