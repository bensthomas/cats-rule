//
//  Reusable.swift
//  CatsRule
//
//  Created by Ben Thomas on 11/3/18.
//

import UIKit

public protocol Reusable: class {
    static var reuseIdentifier: String { get }
    static var nib: UINib? { get }
}

public extension Reusable {
    static var reuseIdentifier: String {
        return String(describing: Self.self)
    }

    static var nib: UINib? {
        let bundle = Bundle(for: Self.self)
        return UINib(nibName: String(describing: Self.self), bundle: bundle)
    }
}
