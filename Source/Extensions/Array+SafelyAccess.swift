//
//  Array+SafelyAcces.swift
//  CatsRule
//
//  Created by Ben Thomas on 12/3/18.
//

import Foundation

extension Array {
    func safelyGetValue(at index: Int) -> Element? {
        guard self.indices.contains(index) else { return nil }
        return self[index]
    }
}
