//
//  PetModelMapper.swift
//  CatsRule
//
//  Created by Ben Thomas on 12/3/18.
//

import Foundation

typealias Gender = String
typealias CatName = String
typealias UnsortedGenderModel = [Gender: [CatName]]
typealias GenderModel = [(key: UnsortedGenderModel.Key, value: UnsortedGenderModel.Value)]
typealias ResponseModel = [PersonModel]

private enum PetType: String {
    case Cat
}

final class PetModelMapper {
    private static let unknownGenderTitle = "Unknown gender"

    public func map(responseModel: ResponseModel) -> GenderModel {
        var dataModel: UnsortedGenderModel = [:]

        responseModel.forEach { person in
            // filter out the non-cats
            guard let cats = getCatsOnly(from: person.pets) else { return }

            let gender = person.gender ?? PetModelMapper.unknownGenderTitle

            if let _ = dataModel[gender] {
                dataModel[gender]?.append(contentsOf: cats)
            } else {
                dataModel[gender] = cats
            }
        }

        let sortedGenderModel = dataModel.sorted(by: { $0.key > $1.key })

        return sortedGenderModel
    }

    public func getDataModelWithSortedCatNames(in dataModel: GenderModel) -> GenderModel {
        var genderModelWithSortedCatNames: GenderModel = []

        genderModelWithSortedCatNames = dataModel.map {
            return ($0.key, $0.value.sorted())
        }

        return genderModelWithSortedCatNames
    }

    // public so we can unit test it. Yuck.

    public func getCatsOnly(from pets: [PetModel]?) -> [CatName]? {
        return pets?.flatMap {
            guard let petType = $0.type,
                PetType(rawValue: petType) == .Cat else { return nil }
            return $0.name
        }
    }
}
