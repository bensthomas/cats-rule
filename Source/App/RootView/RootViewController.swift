//
//  ViewController.swift
//  CatsRule
//
//  Created by Ben Thomas on 11/3/18.
//

import UIKit

class RootViewController: UIViewController {

    // MARK: - Outlets

    @IBOutlet private weak var tableView: UITableView!

    // MARK: - Properties

    private let viewModel = RootViewModel()

    // MARK: - UIViewController

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)

        viewModel.getCats(success: { [weak self] in
            self?.tableView.reloadData()
        }, failure: { errorDescription in
            print(errorDescription)
            // show alert view or some other error
        })
    }
}

// MARK: - UITableViewDataSource

extension RootViewController: UITableViewDataSource {
    func numberOfSections(in tableView: UITableView) -> Int {
        return viewModel.numberOfSections
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let numberOfRows = viewModel.numberOfRows(in: section) else {
            assertionFailure()
            return 0
        }

        return numberOfRows
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: CatCell.reuseIdentifier, for: indexPath)

        guard let catCellViewModel = viewModel.cellViewModelForRow(at: indexPath),
            let catCell = cell as? CatCell else {
                assertionFailure()
                return cell
        }

        catCell.configure(with: catCellViewModel)

        return catCell
    }

    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        guard let headerTitle = viewModel.title(for: section) else {
            assertionFailure()
            return nil
        }

        return headerTitle
    }
}

// MARK: - UITableViewDelegate

extension RootViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, willDisplayHeaderView view: UIView, forSection section: Int) {
        guard let headerView = view as? UITableViewHeaderFooterView else {
            assertionFailure()
            return
        }

        headerView.backgroundView?.backgroundColor = UIColor(red: 0.023, green: 0.051, blue: 0.300, alpha: 1)
        headerView.textLabel?.textColor = UIColor(red: 0.484, green: 0.905, blue: 0.937, alpha: 1)
    }
}
