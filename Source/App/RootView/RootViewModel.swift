//
//  RootViewModel.swift
//  CatsRule
//
//  Created by Ben Thomas on 11/3/18.
//

import Foundation

class RootViewModel {

    // MARK: - Private Properties

    private var model: GenderModel = []
    private let petModelMapper = PetModelMapper()

    // MARK: - Public Properties

    public var numberOfSections: Int {
        return model.count
    }

    // MARK: - Public Methods

    public func title(for section: Int) -> String? {
        return model.safelyGetValue(at: section)?.key
    }

    public func numberOfRows(in section: Int) -> Int? {
        return model.safelyGetValue(at: section)?.value.count
    }

    public func cellViewModelForRow(at indexPath: IndexPath) -> CatCellViewModel? {
        guard let cats: [CatName] = model.safelyGetValue(at: indexPath.section)?.value,
            let catName = cats.safelyGetValue(at: indexPath.row) else {
                return nil
        }

        return CatCellViewModel(catName: catName)
    }
}

// MARK: - Networking

extension RootViewModel {
    public func getCats(success: @escaping () -> (), failure: @escaping (String) -> ()) {
        let gotPets: (Data) -> () = { [weak self] responseData in
            guard let `self` = self else { return }

            do {
                let responseModel: ResponseModel = try JSONDecoder().decode([PersonModel].self, from: responseData)

                self.model = self.petModelMapper.map(responseModel: responseModel)
                self.model = self.petModelMapper.getDataModelWithSortedCatNames(in: self.model)
                success()
            } catch {
                failure("Error decoding JSON")
            }
        }

        let failedToGetPets: (Error) -> () = { error in
            print("Error getting pets: \n \(error)")
            failure(error.localizedDescription)
        }

        // this should be encapsulated in some kind of network service, and that network service should be injected
        // into this class via a network service protocol, so the network can be stubbed in unit tests
        APIClient.shared.get(from: .petOwners, success: gotPets, failure: failedToGetPets)
    }
}

