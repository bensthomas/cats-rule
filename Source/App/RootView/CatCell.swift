//
//  CatCell.swift
//  CatsRule
//
//  Created by Ben Thomas on 11/3/18.
//

import UIKit

class CatCell: UITableViewCell {
    func configure(with viewModel: CatCellViewModel) {
        textLabel?.text = viewModel.catName
    }
}

extension CatCell: Reusable {}
