//
//  AppDelegate.swift
//  CatsRule
//
//  Created by Ben Thomas on 11/3/18.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {
    var window: UIWindow?
}
